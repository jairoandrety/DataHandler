﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jairoandrety.DataHandler
{
    /// <summary>
    /// Extend this class if extra and custom functions are required.
    /// </summary>
    [System.Serializable]
    public class BaseData
    {
        public string date = string.Empty;
        public string content = string.Empty;
    }
}
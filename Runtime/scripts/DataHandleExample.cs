using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;

using UnityEngine;
using jairoandrety.DataHandler;

[System.Serializable]
public class ExampleData
{
    public string nameExample = string.Empty;
    public int countExample = 0;
}

public class DataHandleExample : MonoBehaviour
{
    public ExampleData info = new ExampleData();

    void Start()
    {
        info = new ExampleData();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SaveData();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            LoadData();
        }
    }

    public void SaveData()
    {
        DataHandler.SaveDataJson<ExampleData>(info, "Example.dat");
    }

    public void LoadData()
    {
        BaseData data = new BaseData();
        data = DataHandler.LoadData("Example.dat");
        info = JsonUtility.FromJson<ExampleData>(data.content);
    }
}

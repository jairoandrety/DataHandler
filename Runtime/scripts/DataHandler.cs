﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEditor;
using System.Security.Cryptography;

namespace jairoandrety.DataHandler
{
    public static class DataHandler
    {
        #region Directory
        public static void CreateDirectory(string directoryPath)
        {
            Directory.CreateDirectory(directoryPath);
        }

        private static bool DirectoryExists(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }
        #endregion

        #region File
        public static bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        #region Save
        //public static void SaveDataJson(BaseData data, string directory, string fileName, string extension)
        //{
        //    data.date = DateTime.Now.ToShortDateString();
        //    data.date = Encode(data.date);
        //    data.content = Encode(data.content);

        //    SaveFileData(data, directory, fileName, extension);
        //}

        //public static void SaveDataJson(BaseData data, string fileName, string extension)
        //{
        //    SaveDataJson(data, string.Format("{0}.{1}", fileName, extension));
        //}

        //public static void SaveDataJson(BaseData data, string fileName)
        //{
        //    data.date = DateTime.Now.ToShortDateString();
        //    data.date = Encode(data.date);
        //    data.content = Encode(data.content);

        //    SaveFileData(data, fileName);

        //    //string completePath = Application.persistentDataPath + "/" + fileName + ".dat";
        //    //string json = JsonUtility.ToJson(data, true);
        //    //File.WriteAllText(completePath, json);
        //}

        /// <summary>
        /// Save the data of any class, convert it to json and encrypt it.
        /// </summary>
        /// <typeparam name="T">Any class or data that you want to save</typeparam>
        /// <param name="content">Content of the class or data to be saved</param>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <param name="extension"></param>
        public static void SaveDataJson<T>(T content, string directory, string fileName, string extension)
        {
            BaseData data = new BaseData();
            data.date = "";
            data.content = JsonUtility.ToJson(content);

            data.date = Encode(data.date);
            data.content = Encode(data.content);

            SaveFileData(data, directory, fileName, extension);
        }

        /// <summary>
        /// Save the data of any class, convert it to json and encrypt it.
        /// </summary>
        /// <typeparam name="T">Any class or data that you want to save</typeparam>
        /// <param name="content">Content of the class or data to be saved</param>
        /// <param name="fileName"></param>
        /// <param name="extension"></param>
        public static void SaveDataJson<T>(T content, string fileName, string extension)
        {
            SaveDataJson(content, string.Format("{0}.{1}", fileName, extension));
        }

        /// <summary>
        /// Save the data of any class, convert it to json and encrypt it.
        /// </summary>
        /// <typeparam name="T">Any class or data that you want to save</typeparam>
        /// <param name="content">Content of the class or data to be saved</param>
        /// <param name="fileName"></param>
        public static void SaveDataJson<T>(T content, string fileName)
        {
            BaseData data = new BaseData();
            data.date = "";
            data.content = JsonUtility.ToJson(content);

            data.date = Encode(data.date);
            data.content = Encode(data.content);

            SaveFileData(data, fileName);
        }

        public static void SaveData(byte[] data, string directory, string fileName, string extension)
        {
            string directoryPath = (directory != null) ? Application.persistentDataPath + "/" + directory : Application.persistentDataPath;
            string filePath = directoryPath + "/" + fileName + extension;

            if (!DirectoryExists(directoryPath))
                CreateDirectory(directoryPath);

            try
            {
                File.WriteAllBytes(filePath, data);
                Debug.Log("Saved Data to: " + filePath.Replace("/", "\\"));
            }
            catch (Exception e)
            {
                Debug.LogWarning("Failed To Save Data to: " + filePath.Replace("/", "\\"));
                Debug.LogWarning("Error: " + e.Message);
            }
        }

        private static void SaveFileData(BaseData data, string directory, string fileName, string extension)
        {
            string directoryPath = (directory != null) ? string.Format("{0}/{1}", Application.persistentDataPath, directory) : Application.persistentDataPath;
            //string filePath = string.Format("{0}/{1}.{2}", directoryPath, fileName, extension);
            string filePath = string.Format("{0}/{1}.{2}", directory, fileName, extension);

            if (!DirectoryExists(directoryPath))
                CreateDirectory(directoryPath);

            SaveFileData(data, filePath);

            //BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Create(filePath);
            //Debug.Log(filePath);

            //bf.Serialize(file, data);
            //file.Close();
        }

        private static void SaveFileData(BaseData data, string fileName)
        {
            string filePath = string.Format("{0}/{1}", Application.persistentDataPath, fileName);

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file;

            if (File.Exists(filePath))
            {
                file = File.Open(filePath, FileMode.Open, FileAccess.ReadWrite);
                //file = File.OpenWrite(filePath);
            }
            else
                file = File.Create(filePath);

#if UNITY_EDITOR
            Debug.Log(filePath);
#endif

            bf.Serialize(file, data);
            file.Close();
        }
        #endregion

        #region Load
        public static IEnumerator LoadDataJson(string filePath, Action<BaseData> result)
        {
            BaseData data = new BaseData();
            string completePath = Application.persistentDataPath + "/" + filePath;

            if (FileExists(completePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(completePath, FileMode.Open);
                data = (BaseData)bf.Deserialize(file);
                file.Close();

#if UNITY_EDITOR
                Debug.Log(completePath);
#endif

                data.date = Decode(data.date);
                data.content = Decode(data.content);

                result(data);
                yield return data;
            }
            else
            {
                Debug.Log("Cannot load data! - No Exists");
                //SaveData(data, fileName);
                result(null);
            }
        }

        /// <summary>
        /// Loads the file from the path, deserializes it, and converts it to a BaseData class.
        /// </summary>
        /// <returns>
        /// A standard BaseData class.
        /// </returns>
        /// <param name="fileName"> Full file path. </param>
        public static BaseData LoadData(string filePath)
        {
            BaseData data = new BaseData();
            string completePath = Application.persistentDataPath + "/" + filePath;

            if (FileExists(completePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(completePath, FileMode.Open);
                data = (BaseData)bf.Deserialize(file);
                file.Close();

#if UNITY_EDITOR
                Debug.Log(completePath);
#endif

                data.date = Decode(data.date);
                data.content = Decode(data.content);
                return data;
            }
            else
            {
                Debug.Log("Cannot load data! - No Exists");
                //SaveData(data, fileName);
                return null;
            }
        }
        #endregion

        #region Delete
        public static void DeleteContentFolder(string path)
        {
            FindFolders(path);
            FindFiles(path);
            Directory.Delete(path, true);
        }

        private static void FindFolders(string path)
        {
            var folders = Directory.GetDirectories(Application.persistentDataPath + "/" + path, "*", SearchOption.AllDirectories);
            //var emptyFolders = folders.Where(path => IsEmptyRecursive(path)).ToArray();

            foreach (var folder in folders)
            {
                Debug.Log("folder: " + folder);

                if (Directory.Exists(folder))
                {
                    FindFiles(folder);
                    Debug.Log("Deleting : " + folder);
                    Directory.Delete(folder, true);
                }
            }
        }

        private static void FindFiles(string path)
        {
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path);
                for (int i = 0; i < files.Length; i++)
                {
                    File.Delete(files[i]);
                }
            }
        }
        #endregion
        #endregion

        #region Enconde_Decode
        public static string Encode(string value)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(value);
            string encodedText = Convert.ToBase64String(plainTextBytes);

            return encodedText;
        }

        public static string Decode(string value)
        {
            var encodedTextBytes = Convert.FromBase64String(value);
            string plainText = Encoding.UTF8.GetString(encodedTextBytes);

            return plainText;
        }
        #endregion
    }
}